-- password for users: asdasd
INSERT INTO user (id, username, email, password, role) 
VALUES 
('dwrdbvwpbgrhlyhzccineneqqwnlwgfcohfp', 'admin', 'admin@admin.com', '$2a$10$nYMMY3Q3ShVEgbozL6WELu3z9XIOAvrci3B4p1knulVuX4opBgsbi', 1), 
('ynlpjgsqnrjlqklqevltsrptgsrjdwinkojf', 'author1', 'author1@yournews.com', '$2a$10$nYMMY3Q3ShVEgbozL6WELu3z9XIOAvrci3B4p1knulVuX4opBgsbi', 2), 
('lztqtqoebqmwqfwnltpuhzfcutxlofiibjep', 'author2', 'author2@yournews.com', '$2a$10$nYMMY3Q3ShVEgbozL6WELu3z9XIOAvrci3B4p1knulVuX4opBgsbi', 2), 
('kdvhdbbsqaeazeaxedcpjdleaqevmtosnohr', 'user1', 'user1@yournews.com', '$2a$10$nYMMY3Q3ShVEgbozL6WELu3z9XIOAvrci3B4p1knulVuX4opBgsbi', 3), 
('lplmskboahxbqzuxfdxcrbmbcocigpkbsggk', 'user2', 'user2@yournews.com', '$2a$10$nYMMY3Q3ShVEgbozL6WELu3z9XIOAvrci3B4p1knulVuX4opBgsbi', 3);

INSERT INTO category (name) 
VALUES 
('Government'),
('Sports'),
('Science'),
('Weather');

INSERT INTO news (heading, summary, content, publishingdate, userID, categoryID, draft) 
VALUES 
('First post in sports category', 'This is a test post for sport category', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices odio id efficitur vulputate. Suspendisse quis eros a elit convallis sollicitudin quis eu magna. Duis nibh tortor, ultricies elementum risus sed, blandit blandit sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce posuere dui non orci laoreet suscipit. In in massa sed risus fringilla pellentesque. Etiam auctor sapien nunc, ut interdum ligula condimentum vitae. Nunc consequat nec velit in consectetur. Pellentesque lacinia pellentesque fringilla. Sed sed metus eleifend, consequat arcu sed, rhoncus tortor. Quisque id sapien diam. Morbi placerat augue diam, quis elementum nulla convallis quis. Etiam posuere rhoncus tortor non scelerisque.', '2018-08-18', 'dwrdbvwpbgrhlyhzccineneqqwnlwgfcohfp',2,0),
('Second post in sports category', 'This is a test post for sport category', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices odio id efficitur vulputate. Suspendisse quis eros a elit convallis sollicitudin quis eu magna. Duis nibh tortor, ultricies elementum risus sed, blandit blandit sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce posuere dui non orci laoreet suscipit. In in massa sed risus fringilla pellentesque. Etiam auctor sapien nunc, ut interdum ligula condimentum vitae. Nunc consequat nec velit in consectetur. Pellentesque lacinia pellentesque fringilla. Sed sed metus eleifend, consequat arcu sed, rhoncus tortor. Quisque id sapien diam. Morbi placerat augue diam, quis elementum nulla convallis quis. Etiam posuere rhoncus tortor non scelerisque.', '2018-08-20', 'dwrdbvwpbgrhlyhzccineneqqwnlwgfcohfp',2,0);

INSERT INTO comment (content, date, userID, newsID) 
VALUES 
('That was great!', '2018-08-20', 'dwrdbvwpbgrhlyhzccineneqqwnlwgfcohfp', '1');

