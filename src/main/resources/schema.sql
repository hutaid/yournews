CREATE TABLE IF NOT EXISTS user (
  id VARCHAR(36) PRIMARY KEY,
  username VARCHAR(64) NOT NULL,
  email VARCHAR(64) NOT NULL,
  password VARCHAR(64) NOT NULL,
  role INT NOT NULL,
  UNIQUE KEY UK_email (email),
  UNIQUE KEY UK_username (username)
);

CREATE TABLE IF NOT EXISTS category (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(64) NOT NULL,
	UNIQUE KEY UK_name (name),
);

CREATE TABLE IF NOT EXISTS news (
	id INT AUTO_INCREMENT PRIMARY KEY,
	heading VARCHAR(200) NOT NULL,
	summary TEXT NOT NULL,
	content TEXT NOT NULL,
	publishingdate DATE,
	draft BIT,
	userID VARCHAR(36),
	categoryID INT,
    CONSTRAINT FK_UserNews FOREIGN KEY (userID)
    REFERENCES User(id),
    CONSTRAINT FK_CategoryNews FOREIGN KEY (categoryID)
    REFERENCES category(id)
);

CREATE TABLE IF NOT EXISTS comment (
	id INT AUTO_INCREMENT PRIMARY KEY,
	content TEXT NOT NULL,
	date DATE,
	userID VARCHAR(36),
	newsID INT,
 	CONSTRAINT FK_UserComment FOREIGN KEY (userID)
    REFERENCES User(id),
    CONSTRAINT FK_NewsComment FOREIGN KEY (newsID)
    REFERENCES news(id)
);