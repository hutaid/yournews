package org.learning.yournews.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Category;
import org.learning.yournews.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private final RowMapper<Category> mapper = new RowMapper<Category>() {

    @Override
    public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
      Category n = new Category();
      n.setId(rs.getInt("id"));
      n.setName(rs.getString("name"));
      return n;
    }
  };

  @Override
  public @NotNull List<Category> getCategories() {

    final String sql = "SELECT * FROM category";

    return jdbcTemplate.query(sql, new Object[] {}, mapper);
  }

}
