package org.learning.yournews.repository.impl;

import static org.springframework.util.Assert.notNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Login;
import org.learning.yournews.model.User;
import org.learning.yournews.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {

  private Logger logger = LoggerFactory.getLogger(getClass());

  private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private RowMapper<User> rowMapper = new RowMapper<User>() {
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
      User u = new User();
      u.setUserID(rs.getString("id"));
      u.setUserName(rs.getString("userName"));
      u.setEmailAddress(rs.getString("email"));
      u.setPassword(rs.getString("password"));
      u.setRole(rs.getInt("role"));
      return u;
    }
  };

  @Override
  public void save(@NotNull @Valid User user) {

    logger.info("'save' {} now..." + user);

    final String sql = "INSERT INTO user (id, username, email, password, role) VALUES (?, ?, ?, ?, 3)";
    final String id = UUID.randomUUID().toString();
    final String encodedPassword = passwordEncoder.encode(user.getPassword());

    jdbcTemplate.update(sql, id, user.getUserName(), user.getEmailAddress(), encodedPassword);

  }

  @Override
  public @NotNull User getUserByLogin(Login login) {

    User user = jdbcTemplate.queryForObject("SELECT * FROM user WHERE username = ?",
        new Object[] { login.getUserName()}, rowMapper);

    if (user == null) {
      throw new EmptyResultDataAccessException(1);
    }
    return user;
  }

  @Override
  public User getUserById(String id) {

    notNull(id, "The id can not be null");

    User u = jdbcTemplate.queryForObject("SELECT * FROM user where id = ?", new Object[] { id }, rowMapper);

    if (u == null) {
      throw new EmptyResultDataAccessException(1);
    }
    return u;
  }



}
