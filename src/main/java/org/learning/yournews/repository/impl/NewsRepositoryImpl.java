package org.learning.yournews.repository.impl;

import static org.springframework.util.Assert.notNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Comment;
import org.learning.yournews.model.News;
import org.learning.yournews.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class NewsRepositoryImpl implements NewsRepository {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private RowMapper<News> rowMapper = new RowMapper<News>() {
    public News mapRow(ResultSet rs, int rowNum) throws SQLException {
      News n = new News();
      n.setId(rs.getInt("id"));
      n.setHeading(rs.getString("heading"));
      n.setSummary(rs.getString("summary"));
      n.setContent(rs.getString("content"));
      n.setPublishingDate(rs.getDate("publishingdate"));
      n.setDraft(rs.getBoolean("draft"));
      n.setCategoryID(rs.getInt("categoryID"));
      n.setUserID(rs.getString("userID"));
      return n;
    }
  };
  
  private RowMapper<Comment> rowMapperComment = new RowMapper<Comment>() {
    public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
      Comment n = new Comment();
      n.setId(rs.getInt("id"));
      n.setComment(rs.getString("content"));
      n.setNewsID(rs.getInt("newsID"));
      n.setUserID(rs.getString("userID"));
      return n;
    }
  };

  @Override
  public List<News> getNewsByCategoryID(int categoryID) {

    notNull(categoryID, "The categoryid can not be null");

    return jdbcTemplate.query(
        "SELECT * FROM news WHERE categoryID = ? AND draft = 0 ORDER BY publishingdate DESC LIMIT 20",
        new Object[] { categoryID }, rowMapper);
  }

  @Override
  public News getNewsByID(int id) {

    notNull(id, "The id can not be null");

    return jdbcTemplate.queryForObject("SELECT * FROM news WHERE id = ?", new Object[] { id }, rowMapper);
  }

  @Override
  public void saveArticle(@NotNull @Valid News article) {

    notNull(article, "The entity can not be null");

    jdbcTemplate.update(
        "INSERT INTO news (heading, summary, content, publishingdate, draft, userID, categoryID) VALUES (?, ?, ?, CURDATE(), ?, ?, ?)",
        article.getHeading(), article.getSummary(), article.getContent(), article.getDraft(), article.getUserID(),
        article.getCategoryID());

  }

  @Override
  public List<News> getNewsByUserID(String id) {
    return jdbcTemplate.query("SELECT * FROM news WHERE userID = ? ORDER BY publishingdate DESC", new Object[] { id },
        rowMapper);
  }

  @Override
  public List<News> getAllNews() {
    return jdbcTemplate.query("SELECT * FROM news WHERE draft = FALSE ORDER BY publishingdate DESC", rowMapper);
  }

  @Override
  public void editArticle(@NotNull @Valid News article) {
    notNull(article, "The entity can not be null");
    jdbcTemplate.update("UPDATE news set heading = ?,  summary = ?, content = ?, draft = ?, categoryID = ? where id = ?", article.getHeading(),
        article.getSummary(), article.getContent(), article.getDraft(), article.getCategoryID(), article.getId());

  }

  @Override
  public List<Comment> getCommentsByNewsID(int newsID) {
     
    return jdbcTemplate.query("SELECT * FROM comment WHERE newsID = ?", new Object[] { newsID }, rowMapperComment);
  }

  @Override
  public void saveComment(@NotNull @Valid Comment comment) {
    notNull(comment, "The comment can not be null");

    jdbcTemplate.update(
        "INSERT INTO comment (content, date, userID, newsID) VALUES (?, CURRENT_DATE(), ?, ?)",
       comment.getComment(), comment.getUserID(), comment.getNewsID());
    
  }

}
