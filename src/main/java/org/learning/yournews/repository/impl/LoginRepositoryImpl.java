package org.learning.yournews.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.learning.yournews.model.Login;
import org.learning.yournews.repository.LoginRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class LoginRepositoryImpl implements LoginRepository {

  private Logger logger = LoggerFactory.getLogger(getClass());

  private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private RowMapper<Login> rowMapper = new RowMapper<Login>() {
    @Override
    public Login mapRow(ResultSet rs, int rowNum) throws SQLException {
      Login n = new Login();
      n.setUserName(rs.getString("userName"));
      n.setPassword(rs.getString("password"));
      return n;
    }
  };
  
  @Override
  public boolean getUserByLogin(Login login) {
    logger.info("'Try login' {} now...", login);

    final String sql = "SELECT * FROM user WHERE username = ?";

    List<Login> user = jdbcTemplate.query(sql, rowMapper, login.getUserName());
    
    final boolean passwordMatch = user.size() == 1 ? passwordEncoder.matches(login.getPassword(), user.get(0).getPassword()) : false;
    if(user.size() == 1 && passwordMatch) {
      return true;
    }else {
      return false;
    }
    
  }

}
