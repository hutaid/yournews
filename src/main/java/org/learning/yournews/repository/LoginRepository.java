package org.learning.yournews.repository;

import org.learning.yournews.model.Login;
import org.springframework.validation.annotation.Validated;

@Validated
public interface LoginRepository {

  boolean getUserByLogin(Login login);
}
