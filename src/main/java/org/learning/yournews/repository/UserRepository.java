package org.learning.yournews.repository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Login;
import org.learning.yournews.model.User;
import org.springframework.validation.annotation.Validated;

@Validated
public interface UserRepository {

  void save(@NotNull @Valid User user);
  
  @NotNull
  User getUserByLogin(Login login);
  
  User getUserById(String id);
  
}
