package org.learning.yournews.repository;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Comment;
import org.learning.yournews.model.News;
import org.springframework.validation.annotation.Validated;

@Validated
public interface NewsRepository {
  
  List<News> getNewsByCategoryID(int categoryID);
  
  News getNewsByID(int id);  
  
  List<News> getNewsByUserID(String id);
  
  List<News> getAllNews();
  
  List<Comment> getCommentsByNewsID(int newsID);

  void saveArticle(@NotNull @Valid News article);
  
  void editArticle(@NotNull @Valid News article);
  
  void saveComment(@NotNull @Valid Comment comment);

}
