package org.learning.yournews.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;

public class News {
  
  private int id;
  @NotBlank
  private String heading;
  @NotBlank
  private String summary;
  @NotBlank
  private String content;
  private Date publishingDate;
  private String userID;
  private int categoryID;
  private boolean draft;
  
  public News() {
    
  }
  
  public News(int id, String heading, String summary, String content, Date publishingDate, String userID,
      int categoryID, boolean draft) {
    super();
    this.id = id;
    this.heading = heading;
    this.summary = summary;
    this.content = content;
    this.publishingDate = publishingDate;
    this.userID = userID;
    this.categoryID = categoryID;
    this.draft = draft;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getHeading() {
    return heading;
  }

  public void setHeading(String heading) {
    this.heading = heading;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Date getPublishingDate() {
    return publishingDate;
  }

  public void setPublishingDate(Date publishingDate) {
    this.publishingDate = publishingDate;
  }

  public String getUserID() {
    return userID;
  }

  public void setUserID(String userID) {
    this.userID = userID;
  }

  public int getCategoryID() {
    return categoryID;
  }

  public void setCategoryID(int categoryID) {
    this.categoryID = categoryID;
  }
  
  public boolean getDraft() {
    return draft;
  }

  public void setDraft(boolean draft) {
    this.draft= draft;
  }

  @Override
  public String toString() {
    return "News [id=" + id + ", heading=" + heading + ", summary=" + summary + ", content=" + content
        + ", publishingDate=" + publishingDate + ", userID=" + userID + ", categoryID=" + categoryID + ", draft=" + draft + "]";
  }
   
}
