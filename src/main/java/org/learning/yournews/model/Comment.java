package org.learning.yournews.model;

import javax.validation.constraints.NotBlank;

public class Comment {
  
  private int id;

  @NotBlank
  private String comment;

  private int newsID;

  private String userID;

  public Comment() {

  }

  public Comment(int id, String comment, int newsID, String userID) {
    super();
    this.id = id;
    this.comment = comment;
    this.newsID = newsID;
    this.userID = userID;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public int getNewsID() {
    return newsID;
  }

  public void setNewsID(int newsID) {
    this.newsID = newsID;
  }

  public String getUserID() {
    return userID;
  }

  public void setUserID(String userID) {
    this.userID = userID;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

}
