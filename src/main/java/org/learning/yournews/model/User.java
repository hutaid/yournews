package org.learning.yournews.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.learning.yournews.constraints.EqualFields;

@EqualFields(baseField = "password", matchField = "matchingPassword")
public class User {
  
  private String userID;

  @NotBlank
  @Size(min = 3, max = 30)
  private String userName;
  
  @NotBlank
  @Email
  private String emailAddress;
  
  @NotBlank
  @Size(min = 6, max = 30)
  private String password;
  
  @NotBlank
  @Size(min = 6, max = 30)
  private String matchingPassword;
  
  private int role;
  
  public User() {
    
  }

  public User(String userID, String userName, String emailAddress, String password, String matchingPassword, int role) {
    super();
    this.userID = userID;
    this.userName = userName;
    this.emailAddress = emailAddress;
    this.password = password;
    this.matchingPassword = matchingPassword;
    this.role = role;
  }
  
  public String getUserID() {
    return userID;
  }

  public void setUserID(String userID) {
    this.userID = userID;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getMatchingPassword() {
    return matchingPassword;
  }

  public void setMatchingPassword(String matchingPassword) {
    this.matchingPassword = matchingPassword;
  }

  public int getRole() {
    return role;
  }

  public void setRole(int role) {
    this.role = role;
  }

  @Override
  public String toString() {
    return "User [userID=" + userID + ", userName=" + userName + ", emailAddress=" + emailAddress + ", password="
        + password + ", matchingPassword=" + matchingPassword + ", role=" + role + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
    result = prime * result + ((matchingPassword == null) ? 0 : matchingPassword.hashCode());
    result = prime * result + ((password == null) ? 0 : password.hashCode());
    result = prime * result + role;
    result = prime * result + ((userID == null) ? 0 : userID.hashCode());
    result = prime * result + ((userName == null) ? 0 : userName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    User other = (User) obj;
    if (emailAddress == null) {
      if (other.emailAddress != null)
        return false;
    } else if (!emailAddress.equals(other.emailAddress))
      return false;
    if (matchingPassword == null) {
      if (other.matchingPassword != null)
        return false;
    } else if (!matchingPassword.equals(other.matchingPassword))
      return false;
    if (password == null) {
      if (other.password != null)
        return false;
    } else if (!password.equals(other.password))
      return false;
    if (role != other.role)
      return false;
    if (userID == null) {
      if (other.userID != null)
        return false;
    } else if (!userID.equals(other.userID))
      return false;
    if (userName == null) {
      if (other.userName != null)
        return false;
    } else if (!userName.equals(other.userName))
      return false;
    return true;
  }

  
}
