package org.learning.yournews.controller;

import org.learning.yournews.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class CategoryControllerAdvice {
  
  @Autowired
  private CategoryService categoryService;
  
  @ModelAttribute
  public void displayCategories(Model model) {    
    model.addAttribute("yournewsCategories", categoryService.getCategories());

}
  }
