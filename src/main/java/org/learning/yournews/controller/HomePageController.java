package org.learning.yournews.controller;

import org.learning.yournews.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomePageController {

  @Autowired
  NewsService newsService;
  
  @GetMapping(value = "/")
  public String displayHomePage(Model model) {       
      model.addAttribute("news", newsService.getAllNews());
      return "home";
  }
}
