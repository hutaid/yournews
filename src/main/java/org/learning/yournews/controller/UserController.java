package org.learning.yournews.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.learning.yournews.model.Login;
import org.learning.yournews.model.User;
import org.learning.yournews.service.LoginService;
import org.learning.yournews.service.UserAlreadyExistsException;
import org.learning.yournews.service.UserNotExistsException;
import org.learning.yournews.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

  private static final String REGISTRATION_FORM = "user-registration";

  private static final String REGISTRATION_FORM_ATTRIBUTE = "registrationForm";

  private static final String PATH_REGISTRATION = "/registration";

  private static final String PATH_LOGIN = "/login";

  private static final String LOGIN_FORM_ATTRIBUTE = "loginForm";

  private static final String LOGIN_FORM = "user-login";

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private UserService userService;

  @Autowired
  private LoginService loginService;

  @GetMapping(value = { PATH_REGISTRATION })
  public String getRegistrationForm(@ModelAttribute(REGISTRATION_FORM_ATTRIBUTE) User u) {
    return REGISTRATION_FORM;
  }

  @PostMapping(PATH_REGISTRATION)
  public String submitRegistration(@ModelAttribute(REGISTRATION_FORM_ATTRIBUTE) @Valid User user, BindingResult result,
      HttpServletResponse response) throws UserAlreadyExistsException {
    logger.debug("user submitted this registration: {}", user);
    if (result.hasErrors()) {
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      result.reject("error.incompleteInput");
      return REGISTRATION_FORM;
    } else {
      userService.register(user);
      return "redirect:/successful-registration";
    }
  }

  @GetMapping(value = { PATH_LOGIN })
  public String displayRegistrationPage(@ModelAttribute(LOGIN_FORM_ATTRIBUTE) User u) {
    return LOGIN_FORM;
  }

  @PostMapping(PATH_LOGIN)
  public String submitLogin(@ModelAttribute(LOGIN_FORM_ATTRIBUTE) @Valid Login login, BindingResult result,
      HttpServletResponse response, HttpSession session) throws UserNotExistsException {

    if (loginService.authenticateUser(login)) {
      session.setAttribute("user", userService.getUserByLogin(login));

      return "redirect:/";
    } else {
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      result.reject("error.auth");
      return LOGIN_FORM;

    }

  }

  @GetMapping(value = { "/successful-registration" })
  public String getRegistrationConfirmation() {
    return "successful-registration";
  }

  @GetMapping(value = "/logout")
  public String logout(HttpServletRequest request) {
    HttpSession httpSession = request.getSession();
    httpSession.invalidate();
    return "redirect:/";
  }

}
