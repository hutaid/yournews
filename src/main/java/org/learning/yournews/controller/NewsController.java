package org.learning.yournews.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.learning.yournews.model.Comment;
import org.learning.yournews.model.News;
import org.learning.yournews.model.User;
import org.learning.yournews.service.NewsService;
import org.learning.yournews.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NewsController {

  private static final String PATH_WRITE_ARTICLE = "/write-article";

  private static final String ARTICLE_FORM = "write-article";

  private static final String ARTICLE_FORM_ATTRIBUTE = "articleForm";

  private static final String PATH_EDIT_ARTICLE = "/edit-article";

  private static final String EDIT_FORM = "edit-article";

  private static final String EDIT_FORM_ATTRIBUTE = "editArticleForm";

  @Autowired
  private NewsService newsService;

  @Autowired
  private UserService userService;

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value = "/category.html", method = RequestMethod.GET)
  public String displayNewsByCategory(Model model, @RequestParam(value = "id", required = false) int categoryID) {
    model.addAttribute("yournewsByCategory", newsService.getNewsByCategoryID(categoryID));
    return "category";
  }

  @RequestMapping(value = "/news.html", method = RequestMethod.GET)
  public String displayNewsByID(Model model, @ModelAttribute("commentForm") News article,
      @RequestParam(value = "id", required = false) int id) {
    News articleByID = newsService.getNewsByID(id);
    List<Comment> comments = newsService.getCommentsByNewsID(id);

    model.addAttribute("comments", comments);
    model.addAttribute("yournewsByID", articleByID);
    model.addAttribute("user", userService.getUserByID(articleByID.getUserID()));

    return "news";
  }
  
  @PostMapping(value = "/news.html")
  public String submitComment(@ModelAttribute("commentForm") @Valid Comment comment, BindingResult result,
      HttpServletResponse response) {

    if (result.hasErrors()) {
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      result.reject("error.incompleteInput");
      return "news";
      
    } else {
      newsService.saveComment(comment);
      return "redirect:/news.html?id=" + comment.getNewsID();
      
    }
    
  }

  @GetMapping(value = PATH_WRITE_ARTICLE)
  public String displayWriteArticle(@ModelAttribute(ARTICLE_FORM_ATTRIBUTE) News article) {
    return ARTICLE_FORM;
  }

  @PostMapping(PATH_WRITE_ARTICLE)
  public String submitArticle(@ModelAttribute(ARTICLE_FORM_ATTRIBUTE) @Valid News article, BindingResult result,
      HttpServletResponse response) {
    logger.debug("user submitted this article: {}", article);

    if (result.hasErrors()) {
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      result.reject("error.incompleteInput");
      return ARTICLE_FORM;
    } else {
      newsService.saveArticle(article);
      return "redirect:/";
    }
  }

  @RequestMapping(value = "/edit-news.html", method = RequestMethod.GET)
  public String displayEditArticle(Model model, @ModelAttribute(EDIT_FORM_ATTRIBUTE) News article,
      @RequestParam(value = "id", required = false) int id) {
   
    News e_article = newsService.getNewsByID(id);
    model.addAttribute("news", e_article);
    return EDIT_FORM;
  }

  @PostMapping(value = "/edit-news")
  public String editArticle(Model model, @ModelAttribute(EDIT_FORM_ATTRIBUTE) @Valid News article, BindingResult result, HttpServletResponse response) {
    logger.debug("user edited this article: {}", article);
    
    
    if (result.hasErrors()) {
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      result.reject("error.incompleteInput");
      return EDIT_FORM + "id?=" + article.getId();
    } else {
      newsService.editArticle(article);
      return "redirect:/news.html?id=" + article.getId();
    }
  }

  @GetMapping(value = "/my-articles")
  public String displayMyArticles(Model model, HttpSession session) {
    if (session.getAttribute("user") != null) {
      User u = (User) session.getAttribute("user");
      model.addAttribute("news", newsService.getNewsByUserID(u.getUserID()));
    }

    return "my-articles";
  }
}
