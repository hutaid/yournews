package org.learning.yournews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YournewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(YournewsApplication.class, args);
	}
}
