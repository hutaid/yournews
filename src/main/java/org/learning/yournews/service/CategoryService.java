package org.learning.yournews.service;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Category;
import org.springframework.validation.annotation.Validated;

@Validated
public interface CategoryService {
  
  @NotNull
  List<Category> getCategories();

}
