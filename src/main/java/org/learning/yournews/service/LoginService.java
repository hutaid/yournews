package org.learning.yournews.service;

import org.learning.yournews.model.Login;

public interface LoginService {

  boolean authenticateUser(Login login);
}
