package org.learning.yournews.service.impl;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Comment;
import org.learning.yournews.model.News;
import org.learning.yournews.repository.NewsRepository;
import org.learning.yournews.service.NewsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewsServiceImpl implements NewsService {

  private Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private NewsRepository repository;

  @Override
  public @NotNull List<News> getNewsByCategoryID(int categoryID) {
    return repository.getNewsByCategoryID(categoryID);
  }

  @Override
  public @NotNull News getNewsByID(int id) {
    return repository.getNewsByID(id);
  }

  @Override
  public void saveArticle(@NotNull @Valid News article) {
    logger.info("Saving article: {}", article);

    repository.saveArticle(article);

  }

  @Override
  public @NotNull List<News> getNewsByUserID(String id) {

    return repository.getNewsByUserID(id);

  }

  @Override
  public @NotNull List<News> getAllNews() {
    return repository.getAllNews();
  }

  @Override
  public void editArticle(@NotNull @Valid News article) {
    logger.info("Edit article: {}", article);

    repository.editArticle(article);

  }

  @Override
  public @NotNull List<Comment> getCommentsByNewsID(int newsID) {
    return repository.getCommentsByNewsID(newsID);
  }

  @Override
  public void saveComment(@NotNull @Valid Comment comment) {
    logger.info("Saving comment: {}", comment);

    repository.saveComment(comment);
  }

}
