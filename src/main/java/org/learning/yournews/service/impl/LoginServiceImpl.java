package org.learning.yournews.service.impl;

import org.learning.yournews.model.Login;
import org.learning.yournews.repository.LoginRepository;
import org.learning.yournews.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

  @Autowired
  private LoginRepository repository;

  @Override
  public boolean authenticateUser(Login login) {

    if (repository.getUserByLogin(login)) {
      return true;
    }

    return false;
  }

}
