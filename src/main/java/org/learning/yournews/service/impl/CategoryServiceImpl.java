package org.learning.yournews.service.impl;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Category;
import org.learning.yournews.repository.CategoryRepository;
import org.learning.yournews.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {
    
  @Autowired
  private CategoryRepository repository;

  @Override
  public @NotNull List<Category> getCategories() {
    
    return repository.getCategories();    
  }

}
