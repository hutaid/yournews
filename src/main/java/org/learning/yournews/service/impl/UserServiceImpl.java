package org.learning.yournews.service.impl;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Login;
import org.learning.yournews.model.User;
import org.learning.yournews.repository.UserRepository;
import org.learning.yournews.service.UserAlreadyExistsException;
import org.learning.yournews.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

  private Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private UserRepository repository;

  @Override
  @Transactional(rollbackFor = UserAlreadyExistsException.class)
  public void register(@NotNull @Valid User user) throws UserAlreadyExistsException {

    logger.info("Registering subscription: {}", user);

    try {
      repository.save(user);
    } catch (DuplicateKeyException duplicate) {
      throw new UserAlreadyExistsException(
          "User already exists with email: " + user.getEmailAddress() + "or username: " + user.getUserName());
    }

  }

  @Override
  public User getUserByID(String id) {
    return repository.getUserById(id);
  }

  @Override
  public User getUserByLogin(Login login) {
    return repository.getUserByLogin(login);
  }



}
