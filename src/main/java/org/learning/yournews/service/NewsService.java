package org.learning.yournews.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Comment;
import org.learning.yournews.model.News;
import org.springframework.validation.annotation.Validated;

@Validated
public interface NewsService {
  
  @NotNull
  List<News> getNewsByCategoryID(int categoryID);
  
  @NotNull
  List<News> getNewsByUserID(String id);
  
  @NotNull
  List<News> getAllNews();
  
  @NotNull
  News getNewsByID(int id);
  
  @NotNull
  List<Comment> getCommentsByNewsID(int newsID);
  
  void saveArticle(@NotNull @Valid News article);
  
  void editArticle(@NotNull @Valid News article);
  
  void saveComment(@NotNull @Valid Comment comment);

}
