package org.learning.yournews.service;

public class UserNotExistsException extends Exception {

  public UserNotExistsException(String message) {
    super(message);
  }
}
