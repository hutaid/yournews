package org.learning.yournews.service;

public class UserAlreadyExistsException extends Exception {
  
  public UserAlreadyExistsException(String message) {
    super(message);
  }

}
