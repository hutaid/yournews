package org.learning.yournews.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.learning.yournews.model.Login;
import org.learning.yournews.model.User;
import org.springframework.validation.annotation.Validated;

@Validated
public interface UserService {
  
  void register(@NotNull @Valid User user) throws UserAlreadyExistsException; 
  
  User getUserByID(String id);
  
  User getUserByLogin(Login login) throws UserNotExistsException;
 

}
